from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, recall_score, precision_score
from sklearn.model_selection import train_test_split

# getting the test and train data
from sklearn.neighbors import KNeighborsClassifier

from Dataframes.Pprocessing import processing

newsgroups_train = fetch_20newsgroups(subset='train')

newsgroups_test = fetch_20newsgroups(subset='test')

x_train = newsgroups_train.data
x_test = newsgroups_test.data
x_corpus = x_train + x_test
y_train = newsgroups_train.target
y_test = newsgroups_test.target
y_corpus = np.concatenate((y_train, y_test), axis=None)

# applying pre-processing on test and train data

preprocessed_corpus = []

for document in x_corpus:
    preprocessed_corpus.append(processing(document))

# building the TFIDF vectorizer


vectorizer = TfidfVectorizer()

x_train_tfidf = vectorizer.fit_transform(preprocessed_corpus)

# print(x_train_tfidf)

xtrain, xtest, ytrain, ytest = train_test_split(x_train_tfidf, y_corpus, train_size=0.7)

# print(Xtest, Xtrain)

neigh = KNeighborsClassifier(n_neighbors=2)
neigh.fit(xtrain, ytrain)
predKnn = neigh.predict(xtest)

print(recall_score(ytest, predKnn))
print(precision_score(ytest, predKnn))
