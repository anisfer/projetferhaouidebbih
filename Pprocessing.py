import nltk
from nltk.corpus import stopwords
import string
from nltk.stem import PorterStemmer


def processing(text):
    # remplacement des punctuations avec des espaces vide, sinon le caractére lui même
    text2 = " ".join("".join([" " if ch in string.punctuation else ch for ch in text]).split())

    # tokenization du text en mot, apres l'avoir en phrases

    tokens = [word for sent in nltk.sent_tokenize(text2) for word in nltk.word_tokenize(sent)]

    # conversion de tous les mots en lower case...

    tokens = [word.lower() for word in tokens]

    # removal of stop words

    stops = stopwords.words('english')

    tokens = [token for token in tokens if token not in stops]

    # removing words less than 3 characters

    tokens = [word for word in tokens if len(word) >= 3]

    # stemming using PorterStemmer

    stemmer = PorterStemmer()

    tokens = [stemmer.stem(word) for word in tokens]

    return join_tokens(tokens)


def join_tokens(text):
    return ' '.join(text)
